const fetchStarWarsDetail = async(StarName) => {
        const rootURL = `https://swapi.co/api/${StarName}/`;
        const res = await fetch(rootURL).catch((error) => {
            const s = document.getElementById('pokedex');
            const pokemonHTMLString =
                `<div class="flex-item">
        <p class="flex-item-error">Error: Hubo un problema con la petición Fetch: ${error.message}</p>
      </div>`;
            pokedex.innerHTML = pokemonHTMLString;
        });